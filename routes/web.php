<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

Route::get('/', [PostController::class, 'show'] )->name('main');
Route::post('/', [PostController::class, 'addPost'])->name('add_post');

Route::post('/filter', [PostController::class, 'filter'])->name('filter');

Route::post('/add/{id}', [CommentController::class, 'addComment'])->name('add_comment');

Route::get('login', function () {
    if (Auth::check()) {
        return redirect(route('private'));
    }
    return view('login');
})->name('login');

Route::post('login', [UserController::class, 'login'])->name('login');

Route::get('register', function () {
    if (Auth::check()) {
        return redirect(route('private'));
    }
    return view('auth/register');
})->name('register');

Route::post('register', [UserController::class, 'saveUser']);

Route::get('logout', function () {
    Auth::logout();
    return redirect(route('main'));
});

Route::get('private', function () {
    if (Auth::check()) {
        return view('private');
    }
    return redirect(route('login'));
})->name('private');

Route::post('/post/{id}/like',[PostController::class, 'addLike'])->name('like-post');

Route::get('/post/{id}', [PostController::class, 'showOne'])->name('one_post');
Route::get('/author/author={author}', [PostController::class, 'authorPosts'])->name('author-posts');

Route::post('delete/{id}', [PostController::class, 'deletePost'])->name('delete');

Route::get('edit/{id}', [PostController::class, 'edit'])->name('edit');


Route::post('update/{id}', [PostController::class, 'update'])->name('update');


Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');
Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/');
})->middleware(['auth', 'signed'])->name('verification.verify');
