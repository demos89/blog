<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    public function show()
    {
        $posts = Post::latest()->with('comments')->simplePaginate(25);
        return view('main', compact('posts'));
    }

    public function addPost(Request $request)
    {
        Post::create([
            'author' => Auth::user()->name,
            'title' => $request->title,
            'body' => $request->body
        ]);

        return redirect(route('main'));
    }

    public function addLike(Request $request) {
       $post = Post::where('id', $request->id)->first();
       $count = ($post->likes+1);
       $post->likes = $count;
       $post->save();
       return redirect('');
    }

    public function showOne(Request $request) {
        $post = Post::where('id', $request->id)->first();
        return view('one-post', compact('post'));
    }

    public function authorPosts(Request $request) {
        $posts = Post::where('author', $request->author)->get();
        return view('author-posts', compact('posts'));
    }

    public function filter(Request $request) {
        $filter = $request->filter;
        if (strcmp($filter, 'oldest') == 0) {
            $posts = Post::oldest()->paginate(25);
            return view('main', compact('posts'));
        }

        $posts = Post::latest()->with('comments')->simplePaginate(25);
        return view('main', compact('posts'));
    }

    public function deletePost(Request $request) {
        Post::find($request->id)->delete();
        return redirect(route('main'));
    }

    public function update(Request $request, $id) {
        $post = Post::find($id);

        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->save();

        return redirect(route('main'));
    }

    public function edit($id) {
        $post = Post::find($id);
        return view('edit', compact('post'));
    }
}
