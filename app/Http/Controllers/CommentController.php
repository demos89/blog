<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function addComment(Request $request) {
        Comment::create([
            'author' => 'guest',
            'body' => $request->comment,
            'post_id' => $request->id
        ]);

        return redirect(route('main'));
    }
}
