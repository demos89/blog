<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function saveUser(Request $request) {  //todo validation

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password
        ]);

        if ($user) {
            Auth::login($user);
            return redirect(route('main'));
        }

        return redirect(route('register'))->withErrors([
            'formError' => 'Registration error'
        ]);
    }

    public function login(Request $request) {

        $loginForm = $request->only('email', 'password');
            //dd($loginForm);
        if (Auth::attempt($loginForm)) {
            return redirect(route('private'));
        }

        return redirect(route('login'))->withErrors([
            'email' => 'Login error'
        ]);
    }
}
