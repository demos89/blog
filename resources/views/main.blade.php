@extends('layout')

@section('content')
    <div style="display: flex; align-items: center; margin-left: auto; margin-right: auto;    width: 80%">

        @auth()
            <div class="mb-3" style="width: 100%">

                <form action="{{ route('add_post') }}" method="post">
                    @csrf
                    <input style="margin-top: 10px" class="form-control form-control-lg" type="text" placeholder="Title"
                           aria-label=".form-control-lg example" name="title">
                    <textarea style="margin-top: 10px" class="form-control" id="exampleFormControlTextarea1" rows="3"
                              placeholder="Text"
                              name="body"></textarea>
                    <button style="margin: 10px" type="submit" class="btn btn-primary">Add post</button>
                </form>
            </div>
        @endauth
    </div>


    <div style="align-items: center; margin-left: auto; margin-right: auto; width: 80%; ">
        <form action="{{route('filter')}}" method="POST">
            @csrf
            <select name="filter" id="filter">
                <option>newest</option>
                <option>oldest</option>
            </select>
            <p><input type="submit" value="Sort"></p>

        </form>

        @foreach($posts as $post)
            <div style="border: solid 1px cadetblue; border-radius: 5px; margin-top: 10px; padding: 10px">
                <a href="/post/{{$post->id}}"><h4>Title: {{$post->title}}</h4></a>
                <h6>Author: <a href="{{route('author-posts', $post->author)}}"> {{$post->author}}</a></h6>
                <p>{{$post->body}}</p>

                <form method="post" action="delete/{{$post->id}}">
                    @csrf
                    <button style="margin: 10px" type="submit" class="btn btn-primary btn-like">Delete</button>
                </form>

                <form method="" action="edit/{{$post->id}}">
                    @csrf
                    <button style="margin: 10px" type="submit" class="btn btn-primary" >Edit</button>
                </form>

                <form method="POST" action="{{route('like-post', $post->id)}}">
                    @csrf

                    <button style="margin: 10px" type="submit" class="btn btn-primary btn-like" id="like">Like post
                        | {{$post->likes}}</button>
                </form>
                <div>
                    <h6 style="margin: 10px">Comments:</h6>
                    @foreach($post['comments'] as $comment)

                        <div style="margin: 2px; border: solid 1px lightskyblue">
                            {{$comment->body}}
                        </div>
                        <button style="margin: 10px" type="submit" class="btn btn-primary">Like comment
                            | {{$comment->likes}}</button>
                    @endforeach
                    <form method="post" action="{{route('add_comment', $post->id)}}">
                        @csrf
                        <textarea style="margin-top: 10px" class="form-control" id="exampleFormControlTextarea1"
                                  rows="3"
                                  placeholder="Add comment"
                                  name="comment"></textarea>

                        <button style="margin: 10px" type="submit" class="btn btn-primary">Add comment</button>
                    </form>
                </div>

            </div>
        @endforeach

        <div class="container">
            {{ $posts->links() }}
        </div>
    </div>

@endsection



