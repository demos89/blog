@extends('layout')

@section('content')
    <div style="display: flex; align-items: center; margin-left: auto; margin-right: auto;    width: 80%">

        @auth()
            <div style="margin-top: 20px">
                <h1>Hello {{auth()->user()->name}}</h1>
            </div>
            <div style="margin-top: 150px; margin-left: -230px ">
                <a href="/logout"><button type="submit" class="btn btn-primary">Logout</button></a>
            </div>
        @endauth
    </div>

@endsection



