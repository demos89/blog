<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

<div style="display: flex; align-items: center; margin-left: auto; margin-right: auto;    width: 80%">
    <a style="text-decoration: none" href="/"><h1>Blog</h1></a>
    <a href="/private" style="font-size: 30px; text-decoration: none; display: flex; margin-left: 50%">Profile</a>
    <a href="/login" style="font-size: 30px; text-decoration: none; display: flex; margin-left: 2%">Login</a>
    <a href="/register" style="font-size: 30px; text-decoration: none; display: flex; margin-left: 2%">Registration</a>
</div>

@yield('content')





